---
layout: handbook-page-toc
title: "Analyst Relations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Analyst relations at GitLab

Analyst relations (AR) is generally considered to be a corporate strategy, communications and marketing activity, but at GitLab, because of our mission that everyone can contribute, we view the analyst community as participants as well. The primary owner of Analyst Relations at GitLab is Strategic Marketing, as they are the conduit between the analysts and the internal company information.  

Industry analysts usually focus on corporate or organizational buyers and their needs rather than on those of individual developers. At GitLab we view them as a marketing focus for those markets. They amplify our message to a different group than our traditional developer communities, although there is overlap to some degree between those communities.

## How we interact with the analyst community

Examples of how we engage with analysts include:
- Schedule briefings where we update analysts on our product, our company, our future direction and strategy;
- Answer questions for analyst reports such as Gartner MQs or Forrester Waves that provide in-depth information on product features, customer references and company information;
- Use analyst reports such as Gartner MQs or Forrester Waves that feature GitLab to help enhance or clarify our story for customers and partners;
- Provide our analyst newsletter to update analysts on what GitLab is doing between briefings;
   - [GitLab Analyst Newsletter October 2018](https://docs.google.com/document/d/1MrAsx2UgIQjwbIEplrqvUvZajEzVRuKfZOGhU-qTExw/edit?usp=sharing)
- Schedule inquiries where analysts answer specific questions we have about products, markets, or strategies we want to understand better;
- Schedule consulting days for an extended dive with an analyst into products, markets, or strategies we are working on;
- Invite analysts to participate in webinars, speaking engagements, quotes for media, or other events where an analyst presence would be beneficial; and
- Hire analyst’s market research departments to help us create, run, and interpret survey research that helps us target markets or develop products optimally.

## How we on-board a new analyst to the GitLab Analyst Relations community

- Open an issue in the Product Marketing project using the Analyst On-boarding template that details:
  - who they are
  - best fit for coverage
  - plan for briefing or an introductory inquiry
- Set them up in ARInsights, the third party analyst-tracking software we use
- Add them to the appropriate analyst newsletter list
- Add them to the forthcoming analyst heat map

## How we off-board an analyst from the GitLab Analyst Relations community

- Open an issue in the Product Marketing project using the Analyst Off-boarding template that details:
  - Highlight their change of coverage area or
  - Identify their reason for off-boarding
  - Identify who is their short-term/long-term replacement
  - Notify the PM/PMM team of transition and change to relationship

## Using inquiry with industry analysts

- Inquiry is about a two-way dialogue on a particular topic that GitLab initiates. Appropriate topics for an inquiry include:
     - Asking about a market
     - Getting feedback on something we are doing (e.g. product vision page, competitive document, presentation, market positioning etc.)
     - Asking about what customers are bringing up in inquiry
     - Ask about upcoming research (e.g. a forthcoming Wave or MQ, or area of focus)
     - Asking opinion on something a competitor is doing or has announced
     - Prepare or get feedback from a GitLab briefing and/or demo
- Inquiries generally last 30 minutes and are usually conducted by videoconference. AR will either schedule a zoom call or the analyst will provide a link to Webex for example.
- Questions for an inquiry should be prepared in advance. If the discussion is exhausted before 30 minutes, it is acceptable to end the call early. If more time is needed, it is okay to agree with the analyst to schedule a follow-up inquiry.
- The same inquiry can be posed to multiple analysts and/or to multiple firms if one is looking for a range of feedback.

## How to request and participate in an analyst inquiry
- All inquiries will be initiated by the Analyst Relations department. If you would like to schedule an inquiry, please create an issue in the project gitlab.com>marketing>product marketing and use the template AR-ResearchRequest and fill out the relevant parts for your inquiry.
- Once AR has booked the inquiry, you will receive an invite with a meeting notes attached. Before the inquiry, you are expected to go into the meeting notes and update the questions space with 3-5 questions you think you would like to ask. This is also the document where you will record notes and key takeaways at the end of the call.
- At the time of the inquiry, please connect to the videoconference and open the notes document so you are ready to begin. AR will introduce the attendees to each other and set the stage for the inquiry topic. Once that is done, AR will hand the call over to the inquiry initiator, who will direct the conversation. 
- The AR team has created a slack channel #ar_chat which is for the team on the call to use as a background communication channel during the inquiry to ensure productive use of the interaction.
- If you are a secondary participant on the call, please use the #ar_chat channel in slack or the document to pose additional questions. The AR team member may call on others to verbalize questions if that is appropriate.  Please note, some analyst companies only allow one person to speak and others technically are background only.
- Once the inquiry is finished please add key takeaways to the section below questions in the meeting notes document.
- If a debrief is required, it may be mentioned in the slack channel and those with time can join. This is not required nor always possible, but it is encouraged when necessary.

## AR - How to set up an analyst inquiry
- Inquiry requests should come via an issue under AR-ResearchRequest
- Clarify that the request is clear. If not, use the issue or slack to get clarification of what the requestor is looking for. Clarify whose attendance is mandatory and whose is optional. 
- If the request involves: 
    - Gartner or Forrester, please use the form on their website for inquiry. 
    - IDC, please reach out to Maggie Margossian to help with scheduling. Her email is mmargossian [at] idc [dot] [com]
    - 451, please reach out to Christina Lamb to help with scheduling. Her email is christina.lamb [at] 451research [dot] [com]
    - Redmonk, please reach out to Juliane Leary to help with scheduling. Her email is jleary [at] redmonk [dot] [com]
- For Gartner or Forrester you will receive a link to the calendar for scheduling. Load that calendar in your browser and compare it to the Google calendar to find 30 minutes that works for all required team members. Once you select a date/time, Gartner or Forrester will send a follow-up email with all the information.
- For IDC/451/Redmonk this will involve swapping potential dates and times until a match is found. You can also call if that is convenient. 
- Once you receive a confirmed date/time from the analyst firm, go into the Google calendar and create a new appointment. We cannot use the invite the analyst company sends. A separate invite must be created by AR for internal GitLab folks. Forwarded emails from analyst companies do not always show up on others' Google calendars. 
    - The title naming convention should be Inquiry - analyst firm - topic and if it is Gartner or Forrester, the reference ID they provide for this transaction. (e.g. Inquiry - Gartner - Monitor Vision Doc Review (Ref# 12422433))
    - For Location please specify a GitLab Zoom link or write in "Gartner Webex" or "IDC Bluejeans" etc.
    - Please place this on the Analyst Relations calendar, not your personal calendar
    - The first line of the notes should be "Meeting Notes" with a link to the meeting notes
    - After a separator please put the videoconference details in.
    - Please add internal guests and specify which might be optional. Do not send the invite yet. These are nested procedures.
- Create the Meeting Notes
    - Go into Google Drive to Analyst Relations>analyst-firm-name-folder>Meeting Notes
    - If possible find another meeting notes using the same analyst, or pick the most recent page, open it and make a copy of the file
    - Rename the new file using the date - analyst name - topic 
    - Make sure the analyst bio reflects the correct analyst - if not, go to the analyst firm web page and link the correct bio
    - If there are other links to update, please do so.  Other links may include:
        - a link to the issue for Research Request 
        - a link to any other issue that might have generated the inquiry
        - a link to any documentation to be referenced in the inquiry (such as a vision page or competitive document)
    - Update the list of GitLab attendees
    - Update the Purpose of Inquiry section - this should be brief and can be copied from the Research Request issue
    - Clear out the space for main questions
    - Clear out the space for key takeaways and update DRI (directly responsible individual) to name of inquiry requestor
    - Do not close out this document yet. These are nested procedures.
- Create the Inquiry Issue
    - Go to gitlab.com>product marketing> and start a new issue, use the issue template AR-inquiry
    - Fill it out. Use GitLab handles for attendees. Use date format for inquiry date: year-month-day
    - Go back to Google Drive to the Meeting Notes and hit "share" get a copy of the document link and paste that in the Meeting Notes section
    - Copy any links to supporting documentation that may be relevant such as a link to a web page or document
    - **MARK ALL ISSUES CONFIDENTIAL - Inquiry with Industry Analysts typically involve non-public, proprietary intellectual property and/or other confidential information and we must do our part to keep it that way.**
    - Use the date of the inquiry for the Due date
    - Click "Submit issue". Once the issue is created, assign the inquiry requestor to the issue
    - If there are related issues, go to that section and add them.  
        - Research Request issue
        - Other inquiries on the same topic if this is a series
        - Link to the MQ/Wave Issue or other related research
    - Copy the link to the issue in the browser bar and return to the Meeting Notes page - you are done with the issue for now.
- Update the Inquiry Issue link in the Meeting Notes page.
- Click the "Share" button and make a copy of the Meeting Notes page, **Check to make sure that sharing/access permissions are appropriately set to GitLab internal access - see previous reference to keeping related issues marked CONFIDENTIAL.** You are done with the meeting notes for now.
- Go back to the calendar invite and add the link to where you have written "Meeting Notes".
- Check through the invite to make sure everything is accurate then send it off.
- **RECOMMENDED: Stay hydrated, keep your favorite beverage at hand and full!**
- On Videoconferencing for inquiries:
    - Gartner currently and consistently uses Webex to deliver its interactions, including inquiry. Gartner will provide a Webex link as it confirms the inquiry. You must use this. Please remind GitLab team members of the need to use the Gartner Webex in advance if they are participating and/or planning to demo something. In our experience GitLab team members may require a little extra time to download and install Webex software and/or updates, test that all equipment is functioning with Webex, and otherwise adjust to the Webex service performance which also in our experience has not been as consistent as Zoom. 
    - Forrester/IDC/Redmonk/451 are usually ok with using a GitLab-provided Zoom. Please check with any new analyst that this is okay. If we do use a Zoom, we need to send that to the scheduling entity and they will update the analyst's internal invitation.

## Responding to vendor product comparison reports (e.g. MQs or Waves)

- GitLab will participate in any Wave or MQ to which we are invited to participate. We will commit to our best possible answers for the questionnaires.
- The parts of the team responsible for answering the questionnaire will make this a priority. We will:

   - Present every feature in the best possible light so we have the most defensible chance at high scores.
   - Provide whatever evidence we can that even hints at what the analysts are looking for.
   - When responding to the analyst request, challenge ourselves to find a way to honestly say “yes” and paint the product in the best light possible. Often, at first glance if we think we don’t support a feature or capability, with a bit of reflection and thought we can adapt our existing features to solve the problem at hand.
   - Present our overall solution within the context of the overall market, meaning these exercises are bake-offs; we are being evaluated in comparison to everyone else rather than in comparison to an ideal world. We should take into account the competitive landscape as we craft our position and support it.
   - Our demo should be viewed as a further proof point of our solution and a way to demonstrate things we might not be able to answer the way we'd like in the questionnaire.
   - Even if we don't score well on the product today, we *must* score well on strategy and/or vision.

- Once the report is published, we will create a microsite for each report that details the strengths and weaknesses and provides a link to relevant epics and issues that show how we are working to improve areas mentioned in the report. This becomes the basis for responding to the next iteration of that report, to continue the dialogue with the analysts, as well as to establish or demonstrate our thought leadership or go-to-market in that space. We will also work with the rest of marketing to create appropriate blogs, social media content, and campaign inputs where appropriate.

## Process for responding to vendor product comparison reports (e.g. MQs or Waves)

#### Before the questionnaire arrives:

- GitLab is aware of what MQs or Waves we may be asked to participate in, particularly if we have done so before.  AR should be checking with Forrester/Gartner to see if
    - A Wave/MQ is likely to be repeated
    - The likely time frame for that Wave/MQ to begin
- Once the timeline is determined, 30-45 days in advance AR will
    - Set up an inquiry with the lead analyst(s) to discuss customer inquiries they have received on this topic over the last year and gain insight on where they think the market is at currently
    - Set up a short (30 minute) demo on the relevant part of GitLab to have them aware of our present position
- GitLab usually receives notice 1-2 weeks in advance that a Wave or MQ is forthcoming, in which they can participate.
    - With Forrester, the process may begin with a Now Tech report.  With Gartner, this process may begin with a Market Guide. It may also begin earlier, with an analyst asking for a meeting to discuss what we see in the market.  This happened for example with Chris Condo of Forrester in 2019 with CI.
    - AR works with PMM team to determine lead PMM, lead TMM and lead PM for the project.
    - AR notifies Customer Reference that a request is imminent.

### When the questionnaire arrives:

- AR replies to analyst firm, acknowledging receipt of invitation and agreement to participate, and indicates key participants (AR Manager and lead PMM.)
- AR responsibilities:
    - Create an issue for the report, a place to keep all links, due dates, supporting materials, related issues, and overall conversation
    - Create a slack channel dedicated to this report, inviting all relevant members to the channel, pinning the issue in the channel
    - Forward any Customer Reference material to the CR Manager
    - Send out an invite for any kickoff call that the analyst may run related to this report and create a notes page for the meeting, adding that to the issue
    - Coordinate all activities for deadlines
    - Manage all correspondence between the analyst company and GitLab, including an questions or clarifications, any requests for extension for demo or customer references, any conversation on choosing dates for demos/briefings, and submission of final questionnaire and customer references
    - Coordinate with team for demo/briefing time slot
    - Work with finance and sales ops, for company information
    - Support the team in finding additional help where necessary
    - Start and track issues for marketing, including a blog, changes to the web page, and gating the asset
    - Remind the team of all due dates and hold periodic meetings if necessary to discuss progress and potential issues
    - Create or update the report web page and make it available to PMM/PM to add their commentary - get it approved by the analyst company
    - Work with corporate marketing and PR to decide if the results should be used in a press release or campaign and if reprint rights are required

- PMM responsibilities:
    - read all information provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - own responsibility for coordinating, collating, and organizing all input requirements including:
        - draft questionnaire feedback
        - final questionnaire response
        - briefing presentation to analysts
        - demo for analysts
        - follow up questions for analysts
    - drive the briefing to the analysts
    - coordinate with Product leadership/CEO/others on final version of the questionnaire   

- Technical product marketing responsibilities:
    - read all information provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - work with PMM and PM in creation of demo and briefing
    - drive the demo during the demo and briefing time slot

- PM responsibilities
    - read all informataion provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - provide feedback to PMM on draft questionnaire and demo
    - place the questionnaire and briefing/demo at the top of the work priority list
    - provide first pass answers to the questionnaire to PMM
    - work with PMM to finalize all answers to the questionnaire

### When the draft report arrives:

- AR shares the draft report with the team to check for technical accuracy
- PMM takes lead in verifying/correcting technical accuracy
- PM provides corrections for technical accuracy
- AR works with PMM for company accuracy issues
- AR submits final corrected version to analysts

- AR works with marketing to get social media content approved by analyst company
- AR works with content marketing to get report gated and landing page approved by analyst company
- PMM writes blog post on the report - optional - decided on a case by case basis - to be released when the report is published
- AR creates or modifies the web page for the report then PM/PMM add context of issues and epics where necessary
- PMM and AR create messaging together for Sales - what is the story around this report - to be released when the report is published
- PMM gets a slot on Sales Enablement to discuss the report


### When the report is published:

- AR makes the report available internally and notifies PMM/Product/Sales
  - sets up an inquiry with the analyst company to discuss the results of the report and answer questions


## Accessing analyst reports

Most analyst companies charge for access to their reports.

-   If GitLab purchases reprint rights to a report, then that link will be available here, on the [Analyst Relations web page](/analysts/), and on the relevant product page. Reprint rights are the rights to share the link to the report - these generally last six months to one year.
-   GitLab maintains relationships with some analyst companies that provide us with access to some or all of their relevant research. These reports are for internal use only and sometimes limited to named individuals.  Those reports are generally kept in an internal [GitLab folder for analyst relations](https://drive.google.com/drive/u/0/folders/1oFmtmoXsbjMb6IuPIgIIZ-MG-tLfOjpw). If you are a GitLab team-member and you need access to a particular report, please reach out to [Analyst Relations](mailto:analysts@gitlab.com) and I'll help you find the research you need.

## Analyst reports that can help you deepen your knowledge
As part of XDR and Sales Enablement, some analyst reports are educational - they can help you build your understanding of the market. We've collected some of those articles to share with you here. These are behind the firewall and for use of employees where they have access rights. If you have any questions on access, please contact [Analyst Relations](mailto:analysts@gitlab.com).

[The complete list of documents by topic can be found here.](/handbook/marketing/product-marketing/analyst-relations/sales-training/)

### Which analyst relations team member should I contact?

  - Listed below are areas of responsibility within the analyst relations team:

    - [Joyce](/company/team/#joycetompsett), Analyst Relations Manager
    - [Colin](/company/team/#colinwfletcher), Manager, Market Research and Customer Insights
    - [Ashish](/company/team/#kuthiala), Director PMM
